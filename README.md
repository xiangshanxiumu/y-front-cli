# y-front-cli
> 前端工程搭建脚手架工具

## 安装
````shell
$ npm install -g y-front-cli
// 查看版本好看是否安装成功
$ y-front-cli -V
````

## 使用
```shell
$ y-front-cli create <app-name> <options>
```
