#! /usr/bin/env node
/** #! /usr/bin/env node：node 脚手架命令入口文件，文件以#!开头 代表这个文件被当作一个执行文件，可以当作脚本运行，npm执行的时候默认是使用/usr/bin/node去执行的 */

// 引入Command
const {Command} = require('commander');
const program = new Command();
// 定义命令
program.command('create <app-name>')
    .description('create a new project, 创建一个新项目')
    .option('-f --force', 'overwrite target directory if it exist, 如果创建的目录存在则直接覆盖')
    .option('--vue2', 'vue2 project template')
    .option('--vue3', 'vue3 project template')
    .option('--react', 'react project template')
    .option('--micro-vue2', 'vue2 qiankun template')
    .option('--micro-react', 'react qiankun template')
    .option('--mini-program','mini program template')
    .action((appName, options) => {
        // 用户输入appName, options
        // 引入create执行
        require('../lib/create')(appName, options);
    });

// 版本号
program.version(`v${require('../package.json').version}`, '-V,--version')
    .description('版本号')
    .usage('<command>[option]');

// 解析用户命令传入参数
program.parse(process.argv);
