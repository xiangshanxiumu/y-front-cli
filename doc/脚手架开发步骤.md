# y-front-cli脚手架开发步骤
> npm上已经有人抢先发布过"y-cli"、“yr-cli”了, 只好调整为y-front-cli

## 一、搭建y-front-cli脚手架项目工程
### 1、创建初始化y-front-cli项目
```shell
mkdir y-front-cli
cd y-front-cli
npm init -y

```
### 2、创建命令行文件
```shell
mkdir bin
cd bin
touch cli.js
```
### 3、配置命令行文件
- cli.js抬头配置"#! /usr/bin/env node"
> node脚手架命令入口文件，文件以#!开头，代表这个文件被当作一个执行文件，可以当作脚本运行，npm执行的时候默认是使用/usr/bin/node去执行的.
- package.json配置
> package.json项目配置文件配置bin命令行文件执行入口
```json
{
  "bin": {
    "y-front-cli": "./bin/index.js"
  }
}
```
### 4、验证bin/cli.js
- 创建测试项目y-front-cli-test
```shell
mkdir y-front-cli-test
cd y-front-cli-test
npm init -y
```
- link调试
```shell
// y-front-cli 项目中,建立全局链接
npm link
// y-front-cli-test项目中
npm link y-front-cli
// 查看版本验证是否成功
y-front-cli --version
```
## 二、cli脚手架命令逻辑开发
### 1、commander.js相关辅助依赖安装

## 三、create命令逻辑开发

## 四、inquirer问询交互开发

## 五、模板加载

