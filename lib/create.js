/** create 命令逻辑 */
// node 自带 path依赖
const path = require('path');
// fs的扩展依赖，文件读写操作
const fsExtra = require('fs-extra');
// 设置终端字体颜色
const chalk = require('chalk');
// 添加符号作用
const logSymbols = require('log-symbols');
// 问询交互
const {directoryInquirer, templateInquirer} = require('./help/inquiry');
// util 工具方法
const {checkOptionsTemplate, getTemplateRepo} = require('./help/util');
// downloadTemplate 模板下载
const downloadTemplate = require('./template-handle/downTempalte');

/* templateInquirer handle */
async function templateInquirerHandle(targetDir) {
    const result = await templateInquirer();
    // 模板问询处理
    if(result.templateInquirer) {
        // 通过用户选择的模板选项 获取对应的模板仓库
        const repo = getTemplateRepo(result.templateInquirer);
        // 若该仓库没有配置地址和分支，则提示暂时不支持
        if(!repo.url || !repo.branch) {
            console.log(logSymbols.warning, chalk.yellow('该模板暂不支持！'));
        } else {
            // 下载模板
            await downloadTemplate(`direct:${repo.url}#${repo.branch}`, targetDir, (result) => {
                console.log('downloadTemplate: result', result);
            });
        }
    }
}
/* directoryInquirer handle */
async function directoryInquirerHandle(targetDir, options) {
    const result = await directoryInquirer();
    switch (result.directoryInquirer) {
        case 'remove':
            // 移除 remove / removeSync
            try {
                await fsExtra.remove(targetDir);
                console.log(logSymbols.success, chalk.green('移除完成！'));
            } catch(error) {
                console.log(logSymbols.error, chalk.red('移除异常！'));
            }
            break;
        case 'replace':
            // 替换，先移除，再创建加载
            try {
                // 移除完成后再创建加载
                await fsExtra.remove(targetDir);
                console.log(logSymbols.success, chalk.green('移除完成！'));
                // 判断options里面是否带有模板信息
                const template = checkOptionsTemplate(options);
                // 有则加载模板，无则询问用户选择哪个工程模板直接加载模板到本地
                if(template){
                    // 有模板，直接下载
                    await downloadTemplate(`direct:${template.url}#${template.branch}`, targetDir, (result) => {
                        // console.log('downloadTemplate: result', result);
                    });
                } else {
                    // 命令里面不带模板信息，则询问用户选择模板后加载工程模板
                    await templateInquirerHandle(targetDir);
                }
            } catch(error) {
                // console.log(error);
            }
            break;
        default:
            // cancel 取消 不做后续处理
            return false;
    }
}

/* create逻辑 */
module.exports = async function(name, options) {
    // 执行目录
    const cwd = process.cwd();
    // 需要创建的目录地址
    const targetDir = path.join(cwd, name);

    // 校验目标目录是否存在
    if(await fsExtra.exists(targetDir)) {
        // 是否强制创建
        if(options?.force) {
            // 强制创建，先删除
            await fsExtra.remove(targetDir);
            // 判断options里面是否带有模板信息
            const template = checkOptionsTemplate(options);
            // 有则加载模板，无则询问用户选择哪个工程模板直接加载模板到本地
            if(template){
                // 有模板，直接下载
                await downloadTemplate(`direct:${template.url}#${template.branch}`, targetDir, (result) => {
                    // console.log('downloadTemplate: result', result);
                });
            } else {
                // 命令里面不带模板信息，则询问用户选择模板后加载工程模板
                await templateInquirerHandle(targetDir);
            }
        } else {
            // 非强制创建
            await directoryInquirerHandle(targetDir, options)
        }
    } else {
        //  判断options里面是否带有模板信息
        const template = checkOptionsTemplate(options);
        // console.log('create.js=>checkOptionsTemplate: template2', template);
        // 有则加载模板
        if(template){
            // 有模板，直接下载
            await downloadTemplate(`direct:${template.url}#${template.branch}`, targetDir,(result) => {
                // console.log('downloadTemplate: result', result);
            });
        } else {
            // 无则询问用户选择哪个工程模板，用户选择模板后，再加载模板到本地
            await templateInquirerHandle(targetDir);
            // const result = await templateInquirer();
            // console.log('create.js=>templateInquirer: result2 ', result);
            // // 模板问询处理
            // if(result.templateInquirer){
            //     // 通过用户选择的模板选项 获取对应的模板仓库
            //     const repo = getTemplateRepo(result.templateInquirer);
            //     console.log('create.js=>getTemplateRepo: repo ', repo);
            //     // 若该仓库没有配置地址和分支，则提示暂时不支持
            //     if(!repo.url || !repo.branch) {
            //         console.log('该模板暂不支持！');
            //     } else {
            //         // 下载模板
            //         await downloadTemplate(`direct:${repo.url}#${repo.branch}`, targetDir);
            //     }
            // }
        }
    }
}
