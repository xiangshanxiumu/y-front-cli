/** 问询交互 */
const inquirer = require('inquirer');

// type：表示提问的类型，包括：input、confirm、 list、rawlist、expand、checkbox、password、 - editor。
// name: 存储当前输入的值。
// message：问题的描述。
// default：默认值。
// choices：列表选项，在某些type下可用，并且包含一个分隔符(separator)；
// validate：对用户的回答进行校验。
// filter：对用户的回答进行过滤处理，返回处理后的值。
// when：根据前面问题的回答，判断当前问题是否需要被回答。
// pageSize：修改某些type类型下的渲染行数。
// prefix：修改message默认前缀。
// suffix：修改message默认后缀。
/*  下载工程模板问询 */
module.exports.templateInquirer = function () {
    return new Promise((resolve, reject) => {
        // 仓库工程模板
        const gitRepo = require('../config/git-repo');
        const options = [
            {
                type: "rawlist",
                message: "请选择一个模板：",
                name: "templateInquirer",
                default: "",
                choices: []
            }
        ];
        for(let item of gitRepo){
            options[0].choices.push({
                key: item.value,
                value: item.value
            });
        }
        options[0].default = gitRepo[0].value;
        inquirer.prompt(options).then((answer) => {
            resolve(answer);
        }).catch((error) => {
            reject(error);
        });
    });
};
/* 询问用户已经存在的目录，怎么处理 */
module.exports.directoryInquirer = function () {
    return new Promise((resolve, reject) =>{
        const options = [
            {
                type: "list",
                message: "项目已存在，请选择处理方案：",
                name: "directoryInquirer",
                default: "cancel",
                choices: [
                    // 替换当前目录/移除已有目录/取消当前操作
                    {name:'取消当前操作', value:'cancel'},
                    {name:'移除已有目录', value:'remove'},
                    {name:'替换当前目录', value:'replace'}
                ]
            }
        ];
        inquirer.prompt(options).then((answer) => {
            resolve(answer);
        }).catch((error) => {
            reject(error);
        });
    });
}
