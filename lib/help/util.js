/** 其他工具方法 */
const gitRepo = require('../config/git-repo');

// 判断options里面是否带有模板参数
module.exports.checkOptionsTemplate = function (options){
    for(let item of gitRepo) {
        // 遍历代码仓库
        if(options[item.value]){
            // 模板存在
            return item;
        }
    }
    return null;
}
// 查找模板仓库
module.exports.getTemplateRepo = function (templateName){
    for(let item of gitRepo) {
        // 遍历代码仓库
        if(templateName === item.value){
            // 模板存在
            return item;
        }
    }
    return null;
}


