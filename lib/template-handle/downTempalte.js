/** 直接从git仓库下载模板方案 */
// download-git-repo
const download = require(`download-git-repo`);

// 通过 direct:url#my-branch指定分枝
module.exports = function (url, dir, options = { clone: true }, cb = null) {
    // download('direct:https://gitlab.com/flippidippi/download-git-repo-fixture.git#my-branch', 'test/tmp', { clone: true }, function (err) {
    //     console.log(err ? 'Error' : 'Success')
    // });
    return new Promise((resolve, reject) => {
        try {
            download(url, dir, options, function (err) {
                // console.log(err ? 'Error' : 'Success')
                cb && cb(err);
                if('Success' === err) {
                    resolve(err);
                } else {
                    reject(err);
                }
            });
        } catch(error){
            // error
            reject(error);
        }
    });
    // try {
    //     download(url, dir, options, function (err) {
    //         cb && cb(err);
    //     });
    // } catch(error){
    //     // error
    // }
}
